package com.example.EmployeeTaofik.DTOs;

public class KategoriKemampuanDTO {
	private long idKategori;
	private String namaKategori;
	public KategoriKemampuanDTO(long idKategori, String namaKategori) {
		super();
		this.idKategori = idKategori;
		this.namaKategori = namaKategori;
	}
	public KategoriKemampuanDTO() {
		super();
	}
	public long getIdKategori() {
		return idKategori;
	}
	public void setIdKategori(long idKategori) {
		this.idKategori = idKategori;
	}
	public String getNamaKategori() {
		return namaKategori;
	}
	public void setNamaKategori(String namaKategori) {
		this.namaKategori = namaKategori;
	}
	
	
}
