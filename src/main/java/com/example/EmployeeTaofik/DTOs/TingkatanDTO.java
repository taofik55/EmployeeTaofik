package com.example.EmployeeTaofik.DTOs;

public class TingkatanDTO {
	private long idTingkatan;
	private String namaTingkatan;
	public long getIdTingkatan() {
		return idTingkatan;
	}
	public void setIdTingkatan(long idTingkatan) {
		this.idTingkatan = idTingkatan;
	}
	public String getNamaTingkatan() {
		return namaTingkatan;
	}
	public void setNamaTingkatan(String namaTingkatan) {
		this.namaTingkatan = namaTingkatan;
	}
	public TingkatanDTO() {
		super();
	}
	public TingkatanDTO(long idTingkatan, String namaTingkatan) {
		super();
		this.idTingkatan = idTingkatan;
		this.namaTingkatan = namaTingkatan;
	}
}
