package com.example.EmployeeTaofik.DTOs;

public class KemampuanDTO {
	private long idKemampuan;
	private KategoriKemampuanDTO kategoriKemampuan;
	private String namaKemampuan;
	public KemampuanDTO(long idKemampuan, KategoriKemampuanDTO kategoriKemampuan, String namaKemampuan) {
		super();
		this.idKemampuan = idKemampuan;
		this.kategoriKemampuan = kategoriKemampuan;
		this.namaKemampuan = namaKemampuan;
	}
	public KemampuanDTO() {
		super();
	}
	public long getIdKemampuan() {
		return idKemampuan;
	}
	public void setIdKemampuan(long idKemampuan) {
		this.idKemampuan = idKemampuan;
	}
	public KategoriKemampuanDTO getKategoriKemampuan() {
		return kategoriKemampuan;
	}
	public void setKategoriKemampuan(KategoriKemampuanDTO kategoriKemampuan) {
		this.kategoriKemampuan = kategoriKemampuan;
	}
	public String getNamaKemampuan() {
		return namaKemampuan;
	}
	public void setNamaKemampuan(String namaKemampuan) {
		this.namaKemampuan = namaKemampuan;
	}
	
	
}
