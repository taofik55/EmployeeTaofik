package com.example.EmployeeTaofik.DTOs;

public class PosisiDTO {
	private long idPosisi;
	private String namaPosisi;
	public long getIdPosisi() {
		return idPosisi;
	}
	public void setIdPosisi(long idPosisi) {
		this.idPosisi = idPosisi;
	}
	public String getNamaPosisi() {
		return namaPosisi;
	}
	public void setNamaPosisi(String namaPosisi) {
		this.namaPosisi = namaPosisi;
	}
	public PosisiDTO() {
		super();
	}
	public PosisiDTO(long idPosisi, String namaPosisi) {
		super();
		this.idPosisi = idPosisi;
		this.namaPosisi = namaPosisi;
	}
}
