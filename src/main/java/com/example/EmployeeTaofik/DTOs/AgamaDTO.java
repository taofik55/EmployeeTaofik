package com.example.EmployeeTaofik.DTOs;

public class AgamaDTO {
	private long idAgama;
	private String namaAgama;
	public AgamaDTO(long idAgama, String namaAgama) {
		super();
		this.idAgama = idAgama;
		this.namaAgama = namaAgama;
	}
	public AgamaDTO() {
		super();
	}
	public long getIdAgama() {
		return idAgama;
	}
	public void setIdAgama(long idAgama) {
		this.idAgama = idAgama;
	}
	public String getNamaAgama() {
		return namaAgama;
	}
	public void setNamaAgama(String namaAgama) {
		this.namaAgama = namaAgama;
	}
	
	
}
