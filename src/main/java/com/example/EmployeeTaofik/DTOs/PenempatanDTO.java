package com.example.EmployeeTaofik.DTOs;

import java.math.BigDecimal;

public class PenempatanDTO {
	private long idPenempatan;
	private String kotaPenempatan;
	private BigDecimal umkPenempatan;
	public long getIdPenempatan() {
		return idPenempatan;
	}
	public void setIdPenempatan(long idPenempatan) {
		this.idPenempatan = idPenempatan;
	}
	public String getKotaPenempatan() {
		return kotaPenempatan;
	}
	public void setKotaPenempatan(String kotaPenempatan) {
		this.kotaPenempatan = kotaPenempatan;
	}
	public BigDecimal getUmkPenempatan() {
		return umkPenempatan;
	}
	public void setUmkPenempatan(BigDecimal umkPenempatan) {
		this.umkPenempatan = umkPenempatan;
	}
	public PenempatanDTO() {
		super();
	}
	public PenempatanDTO(long idPenempatan, String kotaPenempatan, BigDecimal umkPenempatan) {
		super();
		this.idPenempatan = idPenempatan;
		this.kotaPenempatan = kotaPenempatan;
		this.umkPenempatan = umkPenempatan;
	}
}
