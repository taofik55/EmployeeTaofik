package com.example.EmployeeTaofik.DTOs;

import java.math.BigDecimal;

public class PresentasiGajiDTO {
	private long idPresentaseGaji;
	private PosisiDTO posisi;
	private Long idTingkatan;
	private BigDecimal besaranGaji;
	private Integer masaKerja;
	public long getIdPresentaseGaji() {
		return idPresentaseGaji;
	}
	public void setIdPresentaseGaji(long idPresentaseGaji) {
		this.idPresentaseGaji = idPresentaseGaji;
	}
	public PosisiDTO getPosisi() {
		return posisi;
	}
	public void setPosisi(PosisiDTO posisi) {
		this.posisi = posisi;
	}
	public Long getIdTingkatan() {
		return idTingkatan;
	}
	public void setIdTingkatan(Long idTingkatan) {
		this.idTingkatan = idTingkatan;
	}
	public BigDecimal getBesaranGaji() {
		return besaranGaji;
	}
	public void setBesaranGaji(BigDecimal besaranGaji) {
		this.besaranGaji = besaranGaji;
	}
	public Integer getMasaKerja() {
		return masaKerja;
	}
	public void setMasaKerja(Integer masaKerja) {
		this.masaKerja = masaKerja;
	}
	public PresentasiGajiDTO() {
		super();
	}
	public PresentasiGajiDTO(long idPresentaseGaji, PosisiDTO posisi, Long idTingkatan, BigDecimal besaranGaji,
			Integer masaKerja) {
		super();
		this.idPresentaseGaji = idPresentaseGaji;
		this.posisi = posisi;
		this.idTingkatan = idTingkatan;
		this.besaranGaji = besaranGaji;
		this.masaKerja = masaKerja;
	}
}
