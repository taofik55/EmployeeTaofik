package com.example.EmployeeTaofik.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.EmployeeTaofik.models.ListKemampuan;

@Repository
public interface ListKemampuanRepository extends JpaRepository<ListKemampuan, Long> {

}
