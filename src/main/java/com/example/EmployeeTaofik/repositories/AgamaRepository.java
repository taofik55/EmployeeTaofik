package com.example.EmployeeTaofik.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.EmployeeTaofik.models.Agama;

@Repository
public interface AgamaRepository extends JpaRepository<Agama, Long> {

}
