package com.example.EmployeeTaofik.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.EmployeeTaofik.models.KategoriKemampuan;

@Repository
public interface KategoriKemampuanRepository extends JpaRepository<KategoriKemampuan, Long> {

}
