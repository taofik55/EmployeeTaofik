package com.example.EmployeeTaofik.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.EmployeeTaofik.models.LemburBonus;
@Repository
public interface LemburBonusRepository extends JpaRepository<LemburBonus, Long> {

}
