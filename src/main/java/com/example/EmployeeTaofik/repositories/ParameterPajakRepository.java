package com.example.EmployeeTaofik.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.EmployeeTaofik.models.ParameterPajak;

@Repository
public interface ParameterPajakRepository extends JpaRepository<ParameterPajak, Long> {

}
