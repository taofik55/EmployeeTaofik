package com.example.EmployeeTaofik.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.EmployeeTaofik.models.Tingkatan;

@Repository
public interface TingkatanRepository extends JpaRepository<Tingkatan, Long> {

}
