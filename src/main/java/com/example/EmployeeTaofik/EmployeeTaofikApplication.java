package com.example.EmployeeTaofik;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeTaofikApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeTaofikApplication.class, args);
	}

}
